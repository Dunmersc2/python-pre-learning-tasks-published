def factors(number):
    # ==============
    # Your code here
    primfac = []
    d = 2
    while d*d <= number:
        while (number % d) == 0:
            primfac.append(d)  # supposing you want multiple factors repeated
            number //= d
        d += 1
    if number > 1:
       primfac.append(number)
    return primfac
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
