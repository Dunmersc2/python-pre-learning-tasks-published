def calculator(a, b, operator):
    # ==============
    # Your code here
    # This function performs additiion
    def add(a, b):
        return a + b
    # This function performs subtraction
    def subtract(a, b):
        return a - b
    # This function performs multiplication
    def multiply(a, b):
        return a * b
    # This function performs division
    def divide(a, b):
        return a / b
    
    if operator == '+':
        print(a,"+",b,"=", add(a,b))
    elif operator == '-':
        print(a,"-",b,"=", subtract(a,b))
    elif operator == '*':
        print(a,"*",b,"=", multiply(a,b))
    elif operator == '/':
        print(a,"/",b,"=", divide(a,b))

    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
